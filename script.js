/* Відповіді: AJAX це абревіатура, яка розшифровується як Asynchronous JavaScript and XML. Це технологія, 
яка об'єднує у собі JS та XML та дозволяє проводити оновлення інтерфейсу користувача без перезавантаження
браузера. Таким чином застосунки стають швидшими та більш зручними для користувача, а для розробника це 
пришвидшує розробку та тестування різних функцій.
*/

let request = fetch("https://ajax.test-danit.com/api/swapi/films");

request.then((result) => result.json())
.then((data) => {
  data.forEach(element => {

    let div = document.createElement("div");
    document.body.appendChild(div);
    
    let id = document.createElement("p")
    id.textContent = element.episodeId;
    div.append(id);
    
    let name = document.createElement("span")
    name.setAttribute('id', 'name')
    name.textContent = element.name;
    div.append(name);

    let charactersList = element.characters
    for (key of charactersList) {
      let url = fetch(key);
      url.then((result) => result.json())
      .then((data) => {
        let characterName = document.createElement('p');
        characterName.textContent = data.name;
        div.children[2].before(characterName);
      })
    }
    
    let crawl = document.createElement("p");    
    crawl.textContent = element.openingCrawl;
    div.append(crawl)
  });
})
